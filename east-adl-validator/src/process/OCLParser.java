package process;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class OCLParser {
	
	private String filePath;
	
	OCLParser(final String fileName){
		filePath = fileName;
	}
	
	public String getContext(){
		String contextResult= "";
		try {
			File contextfile = new File(filePath);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(contextfile);

			doc.getDocumentElement().normalize();

			System.out.println("root of xml file: "
					+ doc.getDocumentElement().getNodeName());
			NodeList contextNodes = doc.getElementsByTagName("context");
			NamedNodeMap namedNodeMap = contextNodes.item(0).getAttributes();
			Node nameAttributeNode = namedNodeMap.getNamedItem("name");
			System.out.println(nameAttributeNode.toString()+" this was the name of the context!!");

			contextResult = nameAttributeNode.toString();
			
			/*for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					System.out.println("Stock Symbol: "
							+ getValue("symbol", element));
					System.out.println("Stock Price: "
							+ getValue("price", element));
					System.out.println("Stock Quantity: "
							+ getValue("quantity", element));
				}
			}
			*/
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return contextResult;
	}

	public String getQuery(){
		String queryResult= "";
		try {
			File contextfile = new File(filePath);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(contextfile);

			doc.getDocumentElement().normalize();

			System.out.println("root of xml file: "
					+ doc.getDocumentElement().getNodeName());
			NodeList contextNodes = doc.getElementsByTagName("context");
			NamedNodeMap namedNodeMap = contextNodes.item(0).getAttributes();
			Node nameAttributeNode = namedNodeMap.getNamedItem("name");
			System.out.println(nameAttributeNode.toString()+" this was the name of the context!!");
			NodeList queriesNodes = doc.getElementsByTagName("queries"); 	
			NodeList queryNodes = doc.getElementsByTagName("query");
			NamedNodeMap valueNodeMap = queryNodes.item(0).getAttributes();
			Node valueAttributeNode = valueNodeMap.getNamedItem("value");
			System.out.println(valueAttributeNode.toString()+" this was the value of the query!!");

			queryResult = valueAttributeNode.toString();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return queryResult;
	}
		
	private void parse() {

	}

	private static String getValue(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag).item(0)
				.getChildNodes();
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
	}
}
