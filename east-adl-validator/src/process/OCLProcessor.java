package process;
import java.io.IOException;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.Query;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.OCLHelper;

import validator.ui.OptionsObject;
import DomainModel.EAST_ADL.Infrastructure.Elements.ElementsPackage;
import DomainModel.EAST_ADL.Timing.TimingPackage;
import DomainModel.EAST_ADL.Timing.impl.TimingImpl;

public class OCLProcessor {

	private OCLParser oclParser = new OCLParser("/Users/Ger/sep12-group6/east-adl-validator/ocl/Timing.xmi");
	
	public boolean validate(TreePath[] treePath) {
		
		OptionsObject options = new OptionsObject();
		options.setSelections(treePath);
		
		this.doOcl(options);
		
		return true;
	}
	
	
	public void doOcl(final OptionsObject options) {

		TreePath[] treePathList = options.getSelections();
		
		XMIResource model = null;
		
		for (TreePath selection : treePathList) {
			model = (XMIResource)selection.getLastSegment();
		}
			
		boolean valid;
		OCLExpression<EClassifier> query = null;
		try {
			// create an OCL instance for Ecore
			OCL<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, Constraint, EClass, EObject> ocl;
			
			ResourceSet resourceSet = new ResourceSetImpl();
			
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
			
			resourceSet.getPackageRegistry().put(ElementsPackage.eNS_URI, ElementsPackage.eINSTANCE);
			
			//EAXMLImpl root = (EAXMLImpl)model.getContents().get(0);
			TimingImpl root = (TimingImpl)model.getContents().get(0);
			
			Resource res = root.eResource();
			
			try {
				res.load(null);
				System.out.println("We did it!!!!!! " + res.getTimeStamp());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE, res);
			System.out.println("Created an instance of the ocl factory");
			// create an OCL helper object
			OCLHelper<EClassifier, ?, ?, Constraint> helper = ocl
					.createOCLHelper();
			
			System.out.println("Created an instance of the ocl helper");
			// set the OCL context classifier
			helper.setContext(TimingPackage.Literals.TIMING_CONSTRAINT);
			System.out.println("Provided the context to the helper");
			query = helper.createQuery("self.shortName");
			System.out.println("Query: " + query.toString());
			
			// use the query expression parsed before to create a Query
			Query<EClassifier, EClass, EObject> eval = ocl.createQuery(query);
			
			TreeIterator ti = res.getAllContents();
			
			while(ti.hasNext()) {
				System.out.println("TI " + ti.next().toString());
				
			}

			//Collection<?> result = (Collection<?>) eval.evaluate(timingConstraint);
			//System.out.println(res.getAllContents().toString());
			
			Object otherObj = eval.evaluate(res);
			Object obj = ocl.evaluate(res, query);
			System.out.println("Evaluation: " + ocl.isInvalid(ocl.evaluate(res, query)));
	
			// record success
			valid = true;
			
			
		} catch (Exception e) {
			// record failure to parse
			valid = false;
			System.err.println("OCL Error: " + e.getLocalizedMessage());
		}
		
	}
}