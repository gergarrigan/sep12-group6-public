package validator.ui;

import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.TreePath;

import process.OCLProcessor;
import extensions.EditorContextMenuExtension;

public class ContextMenuExtension implements EditorContextMenuExtension {

	public void contributeToMenu(IMenuManager menu, final TreePath[] selections) { 
		
		MenuManager newMenu = new MenuManager("Evolvalidator");
		
		final OCLProcessor oclProcessor = new OCLProcessor();
		
		newMenu.add(new Action() {
			@Override
			public void run() {
				
				XMIResource model = null;
				
				for (TreePath selection : selections) {
					model = (XMIResource)selection.getLastSegment();
					System.out.println("Selection: " + model.toString());
				}
				
				VersionSelectionUI versionSelectionUI = new VersionSelectionUI(selections);
				versionSelectionUI.launchApplication();
				
			}
			
			@Override
			public String getText() {
				return "Hello, world";
			}
			
			@Override
			public String getToolTipText() {
				return "Hello, world";
			}
		});
		
		menu.insertAfter("additions", newMenu);
	}
}
