package validator.ui;

import org.eclipse.jface.viewers.TreePath;

/**
 * OptionsObject contains information about the user's selected options.
 * This object is to be returned at the end when the 'Run' button is clicked.
 * 
 * @author Vasileios Karagiorgis
 */
public class OptionsObject {

    private TreePath selections[];
    private String version               = "";
    // Constraints
    private String c_timingConstraints   = "";
    private String c_elements            = "";
    private String c_vehicleFeatureModel = "";
    private String c_functionModeling    = "";
    private String c_systemModeling      = "";
    private String c_environment         = "";
    private String c_dataTypes           = "";
    private String c_generic             = "";
    private String c_requirements        = "";
    private String c_events              = "";
    private String c_behavior            = "";
    private String c_errorModel          = "";
    private String c_timing              = "";
    private String c_variability         = "";
    private String c_featureModeling     = "";
    private String c_safetyRequirements  = "";

    // Constructor
    public OptionsObject() {}
    
    // Getters
    public TreePath[] getSelections() { return selections; }

    public String getVersion() { return version; }

    public String get_c_timingConstraints() { return c_timingConstraints; }

    public String get_c_elements() { return c_elements; }

    public String get_c_vehicleFeatureModel() { return c_vehicleFeatureModel; }

    public String get_c_functionModeling() { return c_functionModeling; }

    public String get_c_systemModeling() { return c_systemModeling; }

    public String get_c_environment() { return c_environment; }

    public String get_c_dataTypes() { return c_dataTypes; }

    public String get_c_generic() { return c_generic; }

    public String get_c_requirements() { return c_requirements; }

    public String get_c_events() { return c_events; }

    public String get_c_behavior() { return c_behavior; }

    public String get_c_errorModel() { return c_errorModel; }

    public String get_c_timing() { return c_timing; }

    public String get_c_variability() { return c_variability; }

    public String get_c_featureModeling() { return c_featureModeling; }

    public String get_c_safetyRequirements() { return c_safetyRequirements; }

    // Setters
    public void setSelections(TreePath[] selections) {
        this.selections = selections;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void set_c_timingConstraints(String c_timingConstraints) {
        this.c_timingConstraints = c_timingConstraints;
    }

    public void set_c_elements(String c_elements) {
        this.c_elements = c_elements;
    }

    public void set_c_vehicleFeatureModel(String c_vehicleFeatureModel) {
        this.c_vehicleFeatureModel = c_vehicleFeatureModel;
    }

    public void set_c_functionModeling(String c_functionModeling) {
        this.c_functionModeling = c_functionModeling;
    }

    public void set_c_systemModeling(String c_systemModeling) {
        this.c_systemModeling = c_systemModeling;
    }

    public void set_c_environment(String c_environment) {
        this.c_environment = c_environment;
    }

    public void set_c_dataTypes(String c_dataTypes) {
        this.c_dataTypes = c_dataTypes;
    }

    public void set_c_generic(String c_generic) {
        this.c_generic = c_generic;
    }

    public void set_c_requirements(String c_requirements) {
        this.c_requirements = c_requirements;
    }

    public void set_c_events(String c_events) {
        this.c_events = c_events;
    }

    public void set_c_behavior(String c_behavior) {
        this.c_behavior = c_behavior;
    }

    public void set_c_errorModel(String c_errorModel) {
        this.c_errorModel = c_errorModel;
    }

    public void set_c_timing(String c_timing) {
        this.c_timing = c_timing;
    }

    public void set_c_variability(String c_variability) {
        this.c_variability = c_variability;
    }

    public void set_c_featureModeling(String c_featureModeling) {
        this.c_featureModeling = c_featureModeling;
    }

    public void set_c_safetyRequirements(String c_safetyRequirements) {
        this.c_safetyRequirements = c_safetyRequirements;
    }
                                                    
}
